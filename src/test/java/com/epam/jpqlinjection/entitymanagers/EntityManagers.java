package com.epam.jpqlinjection.entitymanagers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagers {

    private final EntityManagerFactory emf;

    public EntityManagers() {
        emf = Persistence.createEntityManagerFactory(
                "com.epam.jpqlinjection.entities"
        );
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
}
