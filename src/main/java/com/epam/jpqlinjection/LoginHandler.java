package com.epam.jpqlinjection;

import com.epam.jpqlinjection.entities.User;

import javax.persistence.EntityManager;
import java.util.OptionalLong;
import java.util.stream.Stream;

public class LoginHandler {
    public OptionalLong login(EntityManager entityManager, String username, String password) {

        Stream<User> userStream = entityManager.createQuery(
                        "select u from User u where u.userName = :username and u.password = :password",
                        User.class)
                .setParameter("username", username)
                .setParameter("password", password.toCharArray())
                .getResultStream();

        return userStream.mapToLong(User::getId).findFirst();
    }
}
